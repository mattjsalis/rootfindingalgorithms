#!/bin/bash
failure="$(grep "FAILED" result.txt)"
failure=$failure"$(grep "ERROR" result.txt)"
if test ! -f "result.txt"
then
  exit 1
fi

if test -z "$failure"
then
  exit 0
else
  cat result.txt
  exit 1
fi
