package functor;

import arithmetic.ADouble;
import functor.Functor;

public class WikipediaExampleFunctor implements Functor<ADouble, ADouble> {

    @Override
    public ADouble valueOf(ADouble input) {
        return new ADouble(Math.pow(input.asDouble(), 3.0) - input.asDouble() - 2.0);
    }

    @Override
    public boolean isAcceptableValue(ADouble aDouble) {
        return Math.abs(valueOf(aDouble).asDouble()) < 2*0.0005;
    }

    @Override
    public double getDesiredValue() {
        return 0.0;
    }
}
