package functor;

import arithmetic.ArithmeticType;

public interface Functor<In extends ArithmeticType, Out extends ArithmeticType> {
    Out valueOf(In input);
    boolean isAcceptableValue(In out);

    double getDesiredValue();
}
