package functor;

import arithmetic.ADouble;

public class NarrowDiscreteAcceptanceFunctor implements Functor<ADouble, ADouble>{

    private ADouble lowerAcceptance;
    private ADouble upperAcceptance;
    public static Integer CALL_COUNT = getCallCount();

    private static Integer getCallCount(){
        try{
            return CALL_COUNT + 0;
        } catch(Exception e) {
            return 0;
        }
    }

    public NarrowDiscreteAcceptanceFunctor(double lowerAcceptance, double upperAcceptance){
        this.lowerAcceptance = new ADouble(lowerAcceptance);
        this.upperAcceptance = new ADouble(upperAcceptance);
    }

    @Override
    public ADouble valueOf(ADouble input) {
        CALL_COUNT++;
        if(input.asDouble() > lowerAcceptance.asDouble() && input.asDouble() < upperAcceptance.asDouble()){
            return new ADouble(1.0);
        }
        return new ADouble(0.0);
    }

    @Override
    public boolean isAcceptableValue(ADouble aDouble) {
        return valueOf(aDouble).asDouble() == 1.0;
    }

    @Override
    public double getDesiredValue() {
        return lowerAcceptance.asDouble();
    }
}
