package functor;

import arithmetic.ADouble;
import functor.Functor;

public class SinFunctor implements Functor<ADouble, ADouble> {

    private double targetValue;
    private double acceptanceError;


    public SinFunctor(double targetValue, double acceptanceError){
        this.targetValue = targetValue;
        this.acceptanceError =  acceptanceError;
    }

    @Override
    public ADouble valueOf(ADouble input) {
        return new ADouble(Math.sin(input.asDouble()));
    }

    @Override
    public boolean isAcceptableValue(ADouble aDouble) {
        return Math.abs(valueOf(aDouble).asDouble() - targetValue) < acceptanceError;
    }

    @Override
    public double getDesiredValue() {
        return targetValue;
    }
}
