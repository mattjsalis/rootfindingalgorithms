package utility;

public class Pair<T1, T2> {
    public T1 first;
    public T2 second;

    public Pair(T1 val1, T2 val2){
        first = val1;
        second = val2;
    }
}
