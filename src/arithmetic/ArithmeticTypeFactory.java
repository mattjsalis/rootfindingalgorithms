package arithmetic;

public class ArithmeticTypeFactory {
    @SuppressWarnings("unchecked")
    public static <T extends ArithmeticType> T getArithmeticTypeFromDouble(T arithmetic, double dub){
        T copied = (T) arithmetic.deepCopy(arithmetic.getClass());
        return (T) copied.fromDouble(dub);
    }
}
