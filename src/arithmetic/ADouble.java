package arithmetic;

public class ADouble implements ArithmeticType{

    private Double value;

    public ADouble(double value){
        this.value = value;
    }

    @Override
    public double asDouble() {
        return value;
    }

    @Override
    public ArithmeticType fromDouble(double dub) {
        return new ADouble(dub);
    }

    @Override
    public <T extends ArithmeticType> T deepCopy(Class<T> clazz) {
        if(clazz.equals(ADouble.class)){
            return (T) new ADouble(value);
        }
        return null;
    }
}
