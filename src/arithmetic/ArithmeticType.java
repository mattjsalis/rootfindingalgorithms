package arithmetic;

public interface ArithmeticType  {
    double asDouble();
    ArithmeticType fromDouble(double dub);
    <T extends ArithmeticType> T deepCopy(Class<T> clazz);
}
