package itp;

import arithmetic.ArithmeticType;
import arithmetic.ArithmeticTypeFactory;
import functor.Functor;
import utility.Pair;

import java.util.HashSet;
import java.util.Set;

public class ITP<In extends ArithmeticType, Out extends ArithmeticType> {

    private final double k1;
    private final double k2;
    private final double n0;
    private final Functor<In, Out> f;

    protected ITP(Functor<In, Out> f, double k1, double k2, double n0){
        this.k1 = k1;
        this.k2 = k2;
        this.n0 = n0;
        this.f = f;
    }

    protected double getBisection(In a, In b){
        return (a.asDouble() + b.asDouble())/2.0;
    }

    protected Double getRegulaFalsiPoint(In a, In b){
        double fOfA = calculateFunctor(a).asDouble();
        double fOfB = calculateFunctor(b).asDouble();
        if(Math.signum(fOfA) == Math.signum(fOfB)){
            return null;
        }
        double denominator = fOfB - fOfA;
        return denominator == 0.0 ? null : (a.asDouble()*fOfB - b.asDouble()*fOfA) / (denominator);
    }

    protected Pair<In, In> getUpdatedSearchBounds(In a, In b, double epsilon, double nMax, int j){

        Pair<In, Out> xyITP = getXYITP(a, b, epsilon, nMax, j);
        if(xyITP == null){
            return null;
        }
        In xITPAsArithmetic = xyITP.first;
        Out yITPAsArithmetic = xyITP.second;

        //Adjustment to algorithm such that the functor decides if the value is "good enough"
        if(f.isAcceptableValue(xITPAsArithmetic)){
            return new Pair<>(xITPAsArithmetic, xITPAsArithmetic);
        }

        double yITP = yITPAsArithmetic.asDouble();

        if(yITP > 0.0){
            return new Pair<>(a, xITPAsArithmetic);
        } else if(yITP < 0.0){
            return new Pair<>(xITPAsArithmetic, b);
        }

        return new Pair<>(xITPAsArithmetic, xITPAsArithmetic);

    }

    protected Pair<In, Out> getXYITP(In a, In b, double epsilon, double nMax, int j){

        //Interpolation
        double bisection = getBisection(a, b);
        Double regulaFalsi = getRegulaFalsiPoint(a, b);
        if(regulaFalsi == null){
            return null;
        }
        //Truncation
        double sigma = Math.signum(bisection - regulaFalsi);
        double delta = Math.min(
                k1*Math.pow(Math.abs(b.asDouble() - a.asDouble()), k2),
                Math.abs(bisection - regulaFalsi));
        double estimator = regulaFalsi + sigma*delta;
        //Projection
        double pk = Math.min(epsilon*Math.pow(2.0, nMax-j) - (b.asDouble() - a.asDouble())/2.0,
                Math.abs(estimator - bisection));
        double xITP = bisection - sigma*pk;

        In xITPAsArithmetic = ArithmeticTypeFactory.getArithmeticTypeFromDouble(a, xITP);
        Out yITPAsArithmetic = calculateFunctor(xITPAsArithmetic);
        return new Pair<>(xITPAsArithmetic, yITPAsArithmetic);
    }


    public In getRoot(In lowerBound, In upperBound, double epsilon){
        //Check if this is viable search space, return null if not
        Double regulaFalsi = getRegulaFalsiPoint(lowerBound, upperBound);
        In theRoot = null;
        if(null != regulaFalsi){
            theRoot = getRootNonRecursive( lowerBound,  upperBound,  epsilon);
            if(theRoot != null){
                return theRoot;
            }
        }
        //If the original search space wasn't viable, start splitting it by factors of two until
        // either all search spaces with a width >= epsilon have been evaluated or a root has been found
        int i = 1;
        Set<Double> alreadyAttemptedValues = new HashSet<>();

        do{
            Pair<In, In> newBounds = findViableSearchSpace(lowerBound, upperBound, epsilon, i, alreadyAttemptedValues);
            if(newBounds != null){
                if(newBounds.first.asDouble() == newBounds.second.asDouble()){
                    return newBounds.first;
                }
                regulaFalsi = getRegulaFalsiPoint(newBounds.first, newBounds.second);

                if(null != regulaFalsi){
                    theRoot = getRootNonRecursive( newBounds.first,  newBounds.second,  epsilon);
                    if(theRoot != null){
                        return theRoot;
                    }
                }
            }
            i++;
        }while(true);

    }

    public Pair<In, In> findViableSearchSpace(In lowerBound, In upperBound, double epsilon, int i, Set<Double> alreadyAttemptedValues){
        double range = upperBound.asDouble() - lowerBound.asDouble();
        double fractionSearchSpace = 1/Math.pow(2, i);
        double searchRange = fractionSearchSpace*range;
        double currentVal = lowerBound.asDouble();
        do{
            while(currentVal <= upperBound.asDouble()){
                if(alreadyAttemptedValues.add(currentVal)){
                    //Check if the functor solution for this value is equal
                    // to the functor solution of the value plus another search range
                    In curValAsArithmetic = ArithmeticTypeFactory.getArithmeticTypeFromDouble(lowerBound, currentVal);
                    In curValPlusSearchRangeAsArithmetic = ArithmeticTypeFactory.getArithmeticTypeFromDouble(lowerBound, currentVal+searchRange);
                    Out curValOut = calculateFunctor(curValAsArithmetic);
                    Out curValPlusSearchRangeOut = calculateFunctor(curValPlusSearchRangeAsArithmetic);

                    //Check if we stumbled across an acceptable value
                    if(f.isAcceptableValue(curValAsArithmetic)){
                        return new Pair<>(curValAsArithmetic, curValAsArithmetic);
                    } else if(f.isAcceptableValue(curValPlusSearchRangeAsArithmetic)){
                        return new Pair<>(curValPlusSearchRangeAsArithmetic, curValPlusSearchRangeAsArithmetic);
                    //If the values aren't the same then return the new bounds
                    } else if (Math.abs(curValOut.asDouble() - curValPlusSearchRangeOut.asDouble())  > epsilon
                            && Math.signum(curValOut.asDouble()) != Math.signum(curValPlusSearchRangeOut.asDouble())){
                        return new Pair<>(curValAsArithmetic,curValPlusSearchRangeAsArithmetic);
                    }
                }
                currentVal += searchRange;
            }
            i++;
            fractionSearchSpace = 1/Math.pow(2, i);
            searchRange = fractionSearchSpace*range;
        }while(searchRange <= epsilon);

        return null;
    }

    private In getRootNonRecursive(In lowerBound, In upperBound, double epsilon){

        final double n1_2 = Math.abs(
                Math.log((upperBound.asDouble() - lowerBound.asDouble())/2*epsilon)
                    /Math.log(2.0)
        );
        final double nMax = n1_2+n0;
        In lowerBoundCopy = (In) lowerBound.deepCopy(lowerBound.getClass());
        In upperBoundCopy = (In) upperBound.deepCopy(upperBound.getClass());
        int i = 0;
        while((upperBoundCopy.asDouble() - lowerBoundCopy.asDouble()) > 2*epsilon){
            Pair<In, In> updatedBounds = getUpdatedSearchBounds(lowerBoundCopy, upperBoundCopy, epsilon, nMax, i);
            if(null == updatedBounds){
                return null;
            }
            lowerBoundCopy = updatedBounds.first;
            upperBoundCopy = updatedBounds.second;
            i++;
        }
        if((upperBoundCopy.asDouble() - lowerBoundCopy.asDouble()) > 2*epsilon){
            return null;
        }
        double rootDub = getBisection(lowerBoundCopy, upperBoundCopy);

        return ArithmeticTypeFactory.getArithmeticTypeFromDouble(lowerBound, rootDub);
    }


    protected Out calculateFunctor(In inValue){
        Out out = f.valueOf(inValue);
        double eval = out.asDouble() - f.getDesiredValue();
        return ArithmeticTypeFactory.getArithmeticTypeFromDouble(out, eval);
    }

}
