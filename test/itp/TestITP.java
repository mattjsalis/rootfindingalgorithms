package itp;

import arithmetic.ADouble;
import functor.NarrowDiscreteAcceptanceFunctor;
import functor.SinFunctor;
import functor.WikipediaExampleFunctor;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestITP {

    @Test
    public void testWikipediaExample(){
        WikipediaExampleFunctor f = new WikipediaExampleFunctor();
        ITP<ADouble, ADouble> itpSearcher = new ITP<>(f, 0.1, 2.0, 1.0);
        ADouble theRoot = itpSearcher.getRoot(new ADouble(1.0), new ADouble(2.0), 0.0005);
        assertNotNull(theRoot);
        assertTrue( f.isAcceptableValue(theRoot));
    }

    @Test
    public void testItpWithSinFunctor(){
        double epsilon = 1E-5;
        SinFunctor f = new SinFunctor(0.1, 2*epsilon);
        ITP<ADouble, ADouble> itpSearcher = new ITP<>(f, 0.1, 2.0, 1.0);
        ADouble theRoot = itpSearcher.getRoot(new ADouble(0.0), new ADouble(50.0), epsilon);
        assertNotNull(theRoot);
        assertEquals(Double.valueOf(0.1), Double.valueOf(f.valueOf(theRoot).asDouble()), 2*epsilon);
    }

    @Test
    public void testITPWithNarrowDiscreteAcceptance(){
        NarrowDiscreteAcceptanceFunctor f = new NarrowDiscreteAcceptanceFunctor(10.0, 10.5);
        ITP<ADouble, ADouble> itpSearcher = new ITP<>(f, 0.1, 2.0, 1.0);

        ADouble theRoot = itpSearcher.getRoot(new ADouble(0.0), new ADouble(50.0), 0.005);
        assertNotNull(theRoot);
        System.out.println(NarrowDiscreteAcceptanceFunctor.CALL_COUNT);
        assertTrue( theRoot.asDouble() >= 10.0 && theRoot.asDouble() < 10.5);
    }
}
